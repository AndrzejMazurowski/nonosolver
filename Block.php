<?php

class Block
{
    public const STATUS_UNSOLVED = 0;
    public const STATUS_SOLVED = 1;

    /** @var int */
    private $length;
    /** @var int */
    private $status = self::STATUS_UNSOLVED;

    public function __construct($length)
    {
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}