<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$columnsPattern = new Pattern ([[6],[2,2],[2,2,2],[1,2,1],[1, 6, 1],[1, 6, 1],[1,2,1],[2,2,2],[2,2],[6]]);
$rowsPattern = new Pattern ([[6],[2,2],[2,2,2],[1,2,1],[1, 6, 1],[1, 6, 1],[1,2,1],[2,2,2],[2,2],[6]]);

$gameMap = new GameMap($columnsPattern, $rowsPattern);

$gameMap->drawMap();

$solver = new Solver($gameMap);

$solver->solve();
