<?php

class Pattern
{
    /** @var array */
    private $elements;

    /** @var int */
    private $longestPattern;

    /** @var array */
    private $patterns;

    public function __construct(array $patterns)
    {
        $this->patterns = $patterns;

        foreach ($patterns as $iterator => $singleList) {
            $this->elements[$iterator] = [];
            $this->longestPattern = $this->longestPattern < count($singleList) ? count($singleList) : $this->longestPattern;
            foreach ($singleList as $blockLength) {
                $this->elements[$iterator][] = new Block($blockLength);
            }
        }
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return count($this->elements);
    }

    /**
     * @param int $iterator
     * @return Block[]
     */
    public function getBlocks(int $iterator)
    {
        return $this->elements[$iterator];
    }

    /**
     * @return array
     */
    public function getPatterns(): array
    {
        return $this->patterns;
    }

    /**
     * @return int
     */
    public function getLongestPattern()
    {
        return $this->longestPattern;
    }


}