<?php

class GameMap
{
    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var Pattern */
    private $columnsPattern;

    /** @var Pattern */
    private $rowsPattern;

    /** @var array */
    private $cells;

    /**
     * GameMap constructor.
     * @param Pattern $columnsPattern
     * @param Pattern $rowsPattern
     */
    public function __construct(Pattern $columnsPattern, Pattern $rowsPattern)
    {
        $this->columnsPattern = $columnsPattern;
        $this->rowsPattern = $rowsPattern;
        $this->height = $rowsPattern->getLength();
        $this->width = $columnsPattern->getLength();

        $this->generateCells();
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return Pattern
     */
    public function getColumnsPattern(): Pattern
    {
        return $this->columnsPattern;
    }

    /**
     * @return Pattern
     */
    public function getRowsPattern(): Pattern
    {
        return $this->rowsPattern;
    }

    public function drawMap()
    {
        $this->printColumnPatterns();
        $this->printLine();
        for ($i = 0; $i < $this->height; $i++) {
            $this->printRowPattern($i);
            echo ' |';
            for ($j = 0; $j < $this->width; $j++) {
                echo $this->cells[$i][$j]->isFilled() ? "###" : "   ";
            }

            echo "|\n";
        }
        $this->printLine();
    }

    public function getCell(int $height, int $width): Cell
    {
        return $this->cells[$height][$width];
    }

    private function generateCells()
    {
        for ($i = 0; $i < $this->height; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $this->cells[$i][$j] = new Cell();
            }
        }
    }

    private function printLine()
    {
        $length = $this->rowsPattern->getLongestPattern();
        $tabs = str_pad('', $length * 3, '-');
        echo $tabs . '-+' . str_pad('', $this->width * 3, '-') . "+\n";
    }

    private function printColumnPatterns()
    {
        $length = $this->rowsPattern->getLongestPattern();
        $tabs = str_pad('', $length * 3, ' ');

        $patterns = $this->rowsPattern->getPatterns();
        $length = $this->rowsPattern->getLongestPattern();
        $patternString = '';

        for ($i = 0; $i < $length; $i++) {
            $lineString = $tabs . ' |';
            for ($j = 0; $j < $this->width; $j++) {
                if (!isset($patterns[$j][$i])) {
                    $lineString .= '   ';
                    continue;
                }
                $lineString .= str_pad($patterns[$j][$i], 3, ' ', STR_PAD_LEFT);
            }
            $patternString = $lineString . "|\n" . $patternString;
        }
        echo $patternString;
    }

    private function printRowPattern(int $row)
    {
        $pattern = $this->rowsPattern->getPatterns()[$row];
        $length = $this->rowsPattern->getLongestPattern();
        $patternString = '';
        for ($i = 0; $i < $length; $i++) {
            if (!isset($pattern[$i])) {
                $patternString = '   ' . $patternString;
                continue;
            }
            $patternString = str_pad($pattern[$i], 3, ' ', STR_PAD_LEFT) . $patternString;
        }
        echo $patternString;
    }
}