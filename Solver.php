<?php

class Solver
{
    const VERTICAL = 'v';

    const HORIZONTAL = 'h';

    /** @var GameMap */
    private $gameMap;

    /**
     * Solver constructor.
     * @param GameMap $gameMap
     */
    public function __construct(GameMap $gameMap)
    {
        $this->gameMap = $gameMap;
    }

    public function solve()
    {
        $solve = false;

        $this->fillCertainlyCells(self::VERTICAL);
        $this->fillCertainlyCells(self::HORIZONTAL);

        while ($solve == false) {
            $solve = $this->checkBlocks();
            $this->gameMap->drawMap();
            break;
        }
    }

    private function fillCells($direction, $colOrRow, $from, $to)
    {
        switch ($direction) {
            case self::VERTICAL:
                for ($i = $from; $i < $to; $i++) {
                    $this->gameMap->getCell($i, $colOrRow)->setFilled(true);
                }

                return;
            case self::HORIZONTAL:
                for ($i = $from; $i < $to; $i++) {
                    $this->gameMap->getCell($colOrRow, $i)->setFilled(true);
                }

                return;
            default:
                return;
        }
    }

    private function iteratePattern(Closure $callback, $direction)
    {
        $length = $direction = self::VERTICAL ? $this->gameMap->getHeight() : $this->gameMap->getWidth();
        $pattern = $direction = self::VERTICAL ? $this->gameMap->getRowsPattern() : $this->gameMap->getColumnsPattern();
        for ($i = 0; $i < $length; $i++) {
            if ($callback($pattern, $i) === false) {
                return false;
            }
        }

        return true;
    }

    private function checkBlocks()
    {
        $callback = function (Pattern $pattern, $iterator) {
            foreach ($pattern->getBlocks($iterator) as $block) {
                if ($block->getStatus() == Block::STATUS_UNSOLVED) {
                    return false;
                };
            }
        };

        $check = $this->iteratePattern($callback, self::VERTICAL);

        if (!$check) {
            return false;
        }

        $check = $this->iteratePattern($callback, self::HORIZONTAL);

        if (!$check) {
            return false;
        }

        return true;
    }

    private function fillCertainlyCells($direction)
    {
        $height = $this->gameMap->getHeight();
        $this->iteratePattern(function (Pattern $pattern, $iterator) use ($height, $direction) {
            $lengths = $pattern->getPatterns()[$iterator];
            $sum = array_sum($lengths) + count($lengths) - 1;

            if ($sum == $height) {
                $from = 0;
                foreach($lengths as $key => $length) {
                    $to = $from + $length;
                    $this->fillCells($direction, $iterator, $from, $to);
                    $pattern->getBlocks($iterator)[$key]->setStatus(Block::STATUS_SOLVED);
                    $from = $to + 1;
                }
            }
        }, $direction);
    }
}