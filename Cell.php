<?php

class Cell
{
    /** @var Cell */
    private $topCell;
    /** @var Cell */
    private $bottomCell;
    /** @var Cell */
    private $leftCell;
    /** @var Cell */
    private $rightCell;
    /** @var bool */
    private $filled = false;

    /**
     * @return Cell
     */
    public function getTopCell(): Cell
    {
        return $this->topCell;
    }

    /**
     * @param Cell $topCell
     */
    public function setTopCell(Cell $topCell): void
    {
        $this->topCell = $topCell;
    }

    /**
     * @return Cell
     */
    public function getBottomCell(): Cell
    {
        return $this->bottomCell;
    }

    /**
     * @param Cell $bottomCell
     */
    public function setBottomCell(Cell $bottomCell): void
    {
        $this->bottomCell = $bottomCell;
    }

    /**
     * @return Cell
     */
    public function getLeftCell(): Cell
    {
        return $this->leftCell;
    }

    /**
     * @param Cell $leftCell
     */
    public function setLeftCell(Cell $leftCell): void
    {
        $this->leftCell = $leftCell;
    }

    /**
     * @return Cell
     */
    public function getRightCell(): Cell
    {
        return $this->rightCell;
    }

    /**
     * @param Cell $rightCell
     */
    public function setRightCell(Cell $rightCell): void
    {
        $this->rightCell = $rightCell;
    }

    /**
     * @return bool
     */
    public function isFilled(): bool
    {
        return $this->filled;
    }

    /**
     * @param bool $filled
     */
    public function setFilled(bool $filled): void
    {
        $this->filled = $filled;
    }
}